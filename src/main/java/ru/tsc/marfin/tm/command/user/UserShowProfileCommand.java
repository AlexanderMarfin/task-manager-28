package ru.tsc.marfin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

}
